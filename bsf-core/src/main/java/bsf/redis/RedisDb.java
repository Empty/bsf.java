package bsf.redis;

import redis.clients.jedis.Jedis;
import redis.clients.jedis.JedisPool;

public class RedisDb implements AutoCloseable
{
	private Jedis redisCli = null;
	private JedisPool redisPool = null;

	public RedisDb(Jedis redisClient)
	{
		redisCli = redisClient;
	}

	public RedisDb(Jedis redisClient,JedisPool redispool)
	{
	redisCli = redisClient;
		redisPool = redispool;
	}



	public final Jedis getClient()
	{
		return redisCli;
	}

	/** 
	 获取key,返回string格式

	 @param key
	 @return
	*/
	public final String getValueString(String key)
	{
		String value = redisCli.get(key);
		return value;
	}

	/**
	 获取key,返回byte[]格式

	 @param key
	 @return

	public final byte[] getValueByte(String key)
	{

		byte[] value = redisCli.get(key);
		return value;
	}*/

	/**
	 获取Key,返回泛型T

	 <typeparam name="T"></typeparam>
	 @param key
	 @return

	public final <T> T GetValue(String key)
	{
		T Value = redisCli.<T>Get(key);
		return Value;
	}
	*/
	/**
	 设置一个值 key值重复则覆盖

	 @param key
	 @param value
	 @return

	public final boolean SetValue(String key, byte[] value)
	{
		return redisCli.set(key, value);
	}
	*/
	/**
	 设置一个值 key值重复则覆盖

	 @param key key值
	 @param value value值
	 @param expiresAt 过期时间
	 @return

	public final boolean SetValue(String key, byte[] value, java.util.Date expiresAt)
	{
		return redisCli.Set(key, value, expiresAt);
	}
	*/
	/**
	 设置一个值 key值重复则覆盖

	 @param key key值
	 @param value value值
	 @param timespan 过期时间
	 @return

	public final boolean SetValue(String key, byte[] value, TimeSpan timespan)
	{
		return redisCli.Set(key, value, timespan);
	}*/
	/**
	 设置一个值 Key值重复则覆盖

	 <typeparam name="T"></typeparam>
	 @param key
	 @param value
	 @return

	public final <T> boolean SetValue(String key, T value)
	{
		return redisCli.<T>Set(key, value);
	}*/

	/**
	 设置一个值 key值重复则覆盖

	 <typeparam name="T"></typeparam>
	 @param key key值
	 @param value value值
	 @param expiresAt 过期时间
	 @return

	public final <T> boolean SetValue(String key, T value, java.util.Date expiresAt)
	{
		return redisCli.<T>Set(key, value, expiresAt);
	}*/

	/**
	 设置一个值 key值重复则覆盖

	 @param key key值
	 @param value value值
	 @param timespan 过期时间
	 @return

	public final <T> boolean SetValue(String key, T value, TimeSpan timespan)
	{
		if (T.class == String.class)
		{
			try
			{
				redisCli.SetEntry(key, (String)((value instanceof String) ? value : null), timespan);
				return true;
			}
			catch (java.lang.Exception e)
			{
				return false;
			}
		}
		return redisCli.<T>Set(key, value, timespan);
	}*/

	/**
	 获得某个hash型key下的所有字段

	 @param hashId
	 @return

	public final java.util.ArrayList<String> GetHashFields(String hashId)
	{
		java.util.ArrayList<String> hashFields = redisCli.GetHashKeys(hashId);
		return hashFields;
	}*/
	/**
	 获得某个hash型key下的所有值

	 @param hashId
	 @return

	public final java.util.ArrayList<String> GetHashValues(String hashId)
	{
		java.util.ArrayList<String> hashValues = redisCli.GetHashKeys(hashId);
		return hashValues;
	}
	*/

	/**
	 获得hash型key某个字段的值

	 @param key
	 @param field

	public final String GetHashField(String key, String field)
	{
		String value = redisCli.GetValueFromHash(key, field);
		return value;
	}*/
	/**
	 设置hash型key某个字段的值

	 @param key
	 @param field
	 @param value

	public final void SetHashField(String key, String field, String value)
	{
		redisCli.SetEntryInHash(key, field, value);
	}*/
	/**
	使某个字段增加

	 @param key
	 @param field
	 @return

	public final void SetHashIncr(String key, String field, int incre)
	{
		redisCli.IncrementValueInHash(key, field, incre);

	}*/

	/**
	 hash表批量增加数据

	 @param key
	 @param fields
	 @param values
	 @return

	public final void HMSet(String key, java.util.ArrayList<String> fields, java.util.ArrayList<Object> values)
	{
		byte[][] byteKeys = new byte[fields.size()][];
		byte[][] byteValues = new byte[fields.size()][];
		for (int i = 0; i < fields.size(); i++)
		{
			byteKeys[i] = XXF.Db.LibConvert.StrToBytes(fields.get(i));
			byteValues[i] = XXF.Db.LibConvert.ObjToBytes(values.get(i));
		}
		redisCli.HMSet(key, byteKeys, byteValues);
	}*/

	/**
	 hash表批量获取数据

	 @param key
	 @param fields

	public final byte[][] HMGet(String key, java.util.ArrayList<String> fields)
	{
		byte[][] byteKeys = new byte[fields.size()][];
		for (int i = 0; i < fields.size(); i++)
		{
			byteKeys[i] = XXF.Db.LibConvert.StrToBytes(fields.get(i));
		}
		return redisCli.HMGet(key, byteKeys);
	}
	*/
	/**
	 向list类型数据添加成员，向列表底部(右侧)添加

	 @param Item
	 @param list

	public final void AddItemToListRight(String list, String item)
	{
		redisCli.AddItemToList(list, item);
	}*/
	/**
	 向list类型数据添加成员，向列表顶部(左侧)添加

	 @param list
	 @param item

	public final void AddItemToListLeft(String list, String item)
	{
		redisCli.LPush(list, Encoding.Default.GetBytes(item));
	}*/

	/**
	 将多个值添加至 redis  List中

	 @param list
	 @param items
	 @param seconds

	public final void AddRangeToList(String list, java.util.ArrayList<String> items, int seconds)
	{
		//using (IRedisTransaction trans = redisCli.CreateTransaction())
		//{
		//    try
		//    {
		//        trans.QueueCommand(r => r.AddRangeToList(list, items));
		//        trans.QueueCommand(r => r.ExpireEntryAt(list, dateTime));
		//        trans.Commit();
		//    }
		//    catch
		//    {
		//        trans.Rollback();
		//    }
		//}
		redisCli.AddRangeToList(list, items);
		redisCli.Expire(list, seconds);

	}*/

	/**
	 将多个值添加至 redis  List中

	 @param list
	 @param items
	 @param dateTime

	public final void ReAddRangeToList(String list, java.util.ArrayList<String> items, int seconds)
	{
		redisCli.Remove(list);
		redisCli.AddRangeToList(list, items);
		redisCli.Expire(list, seconds);
	}*/

	/**
	 将多个值添加至 redis  List中

	 @param list
	 @param items
	 @param dateTime

	public final void ReAddRangeToList(String list, java.util.ArrayList<String> items)
	{
		redisCli.Remove(list);
		redisCli.AddRangeToList(list, items);
	}*/
	/**
	 从list类型数据读取所有成员


	public final java.util.ArrayList<String> GetAllItems(String list)
	{
		java.util.ArrayList<String> listMembers = redisCli.GetAllItemsFromList(list);
		return listMembers;
	}*/
	/**
	 从list类型数据指定索引处获取数据，支持正索引和负索引

	 @param list
	 @return

	public final String GetItemFromList(String list, int index)
	{
		String item = redisCli.GetItemFromList(list, index);
		return item;
	}*/
	/**
	 从列表中批量获取数据

	 @param list
	 @param values

	public final java.util.ArrayList<String> GetRangeToList(String list, int sindex, int eindex)
	{
		java.util.ArrayList<String> lr = redisCli.GetRangeFromList(list, sindex, eindex);
		return lr;
	}*/

	/**
	 从列表中删除指定数量的元素

	 @param list
	 @param count
	 @param value
	 @return

	public final long LRemove(String list, int count, String value)
	{
		return redisCli.LRem(list, count, XXF.Db.LibConvert.StrToBytes(value));
	}*/

	/**
	 向集合中添加数据

	 @param item
	 @param set

	public final void GetItemToSet(String item, String set)
	{
		redisCli.AddItemToSet(item, set);
	}*/
	/**
	 获得集合中所有数据

	 @param set
	 @return

	public final java.util.HashSet<String> GetAllItemsFromSet(String set)
	{
		java.util.HashSet<String> items = redisCli.GetAllItemsFromSet(set);
		return items;
	}*/
	/**
	 获取fromSet集合和其他集合不同的数据

	 @param fromSet
	 @param toSet
	 @return

	public final java.util.HashSet<String> GetSetDiff(String fromSet, String... toSet)
	{
		java.util.HashSet<String> diff = redisCli.GetDifferencesFromSet(fromSet, toSet);
		return diff;
	}*/
	/**
	 获得所有集合的并集

	 @param set
	 @return

	public final java.util.HashSet<String> GetSetUnion(String... set)
	{
		java.util.HashSet<String> union = redisCli.GetUnionFromSets(set);
		return union;
	}*/
	/**
	 获得所有集合的交集

	 @param set
	 @return

	public final java.util.HashSet<String> GetSetInter(String... set)
	{
		java.util.HashSet<String> inter = redisCli.GetIntersectFromSets(set);
		return inter;
	}*/
	/**
	 向有序集合中添加元素

	 @param set
	 @param value
	 @param score

	public final void AddItemToSortedSet(String set, String value, long score)
	{
		redisCli.AddItemToSortedSet(set, value, score);
	}
	*/
	/**
	 获得某个值在有序集合中的排名，按分数的降序排列

	 @param set
	 @param value
	 @return

	public final long GetItemIndexInSortedSetDesc(String set, String value)
	{
		long index = redisCli.GetItemIndexInSortedSetDesc(set, value);
		return index;
	}*/
	/**
	 获得某个值在有序集合中的排名，按分数的升序排列

	 @param set
	 @param value
	 @return

	public final long GetItemIndexInSortedSet(String set, String value)
	{
		long index = redisCli.GetItemIndexInSortedSet(set, value);
		return index;
	}*/
	/**
	 获得有序集合中某个值得分数

	 @param set
	 @param value
	 @return

	public final double GetItemScoreInSortedSet(String set, String value)
	{
		double score = redisCli.GetItemScoreInSortedSet(set, value);
		return score;
	}*/
	/**
	 获得有序集合中，某个排名范围的所有值

	 @param set
	 @param beginRank
	 @param endRank
	 @return

	public final java.util.ArrayList<String> GetRangeFromSortedSet(String set, int beginRank, int endRank)
	{
		java.util.ArrayList<String> valueList = redisCli.GetRangeFromSortedSet(set, beginRank, endRank);
		return valueList;
	}*/
	/**
	 获得有序集合中，某个分数范围内的所有值，升序

	 @param set
	 @param beginScore
	 @param endScore
	 @return

	public final java.util.ArrayList<String> GetRangeFromSortedSet(String set, double beginScore, double endScore)
	{
		java.util.ArrayList<String> valueList = redisCli.GetRangeFromSortedSetByHighestScore(set, beginScore, endScore);
		return valueList;
	}*/
	/**
	 获得有序集合中，某个分数范围内的所有值，降序

	 @param set
	 @param beginScore
	 @param endScore
	 @return

	public final java.util.ArrayList<String> GetRangeFromSortedSetDesc(String set, double beginScore, double endScore)
	{
		java.util.ArrayList<String> vlaueList = redisCli.GetRangeFromSortedSetByLowestScore(set, beginScore, endScore);
		return vlaueList;
	}*/


	/**
	 删除一个key

	 @param key
	 @return

	public final long Delete(String key)
	{
		return redisCli.Del(key);
	}*/

	/**
	 删除一批key

	 @param keys
	 @return

	public final long Deletes(String[] keys)
	{
		return redisCli.Del(keys);
	}

	 */
	public final long Incr(String key)
	{
		return redisCli.incr(key);
	}

	public final void CloseClient()
	{
		dispose();
	}

	public final void dispose()
	{
		/*新版本连接池可以在close中自动回收
		if(redisPool != null)
			redisPool.returnBrokenResource(redisCli);*/
		if (redisCli != null)
		{
			redisCli.close();
		}
	}

	public final void close()
	{
		dispose();
	}

	/**
	 批量数据执行
	 
	 @param command
	 @param onSuccessCallback

	public final void QueueCommand(Action<RedisDb> command, Action onSuccessCallback)
	{
//C# TO JAVA CONVERTER NOTE: The following 'using' block is replaced by its Java equivalent:
//C# TO JAVA CONVERTER TODO TASK: There is no equivalent to implicit typing in Java:
//		using (var pipeline = redisCli.CreatePipeline())
		var pipeline = redisCli.CreatePipeline();
		try
		{
//C# TO JAVA CONVERTER TODO TASK: Lambda expressions and anonymous methods are not converted by C# to Java Converter:
			pipeline.QueueCommand(m =>
				command.invoke(this);
		}
		finally
		{
			pipeline.dispose();
			}
		   , onSuccessCallback);
			pipeline.Flush();
		}
	}*/
}

