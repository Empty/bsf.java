package bsf.system.thread;

import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by chejiangyi on 2017/4/12.
 */
public class ThreadPool {
    //系统默认线程池（模拟.net线程池实现） 对比newCachedThreadPool的差异 设置了最大线程数为200
    public static ThreadPoolExecutor SystemThreadPool = new ThreadPoolExecutor(0, 200,
            60L, TimeUnit.SECONDS,
            new SynchronousQueue<Runnable>(),new SystemThreadPoolFactory());

    static class SystemThreadPoolFactory implements ThreadFactory {
        private ThreadFactory factory = Executors.defaultThreadFactory();

        public Thread newThread(Runnable r) {
            Thread t = factory.newThread(r);
            t.setDaemon(true);//后台线程模式
            return t;
        }
    }

}
