package bsf.system.typeconvert;


/**
 * Created by chejiangyi on 2016/3/21.
 * 类型转换器接口定义
 */
public interface ITypeConvert {
    public <T> T convert(Object t);
}
