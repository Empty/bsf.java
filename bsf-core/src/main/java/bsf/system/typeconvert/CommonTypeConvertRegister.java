package bsf.system.typeconvert;

import bsf.util.ConvertUtil;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by chejiangyi on 2016/3/21.
 * （系统）常用类型转换方式注册
 */
public class CommonTypeConvertRegister {


    public static void register()
    {


        /*Integer - int */
        AutoTypeConvertProvider.registerConverter(Integer.class, int.class, new ITypeConvert() {
            @Override
            public Object convert(Object t) {
                if(t == null)
                    return 0;
                return ((Integer)(t)).intValue();
            }
        });
        AutoTypeConvertProvider.registerConverter(int.class, Integer.class, new ITypeConvert() {
            @Override
            public Object convert(Object t) {
                return (Integer)t;
            }
        });

        /*Byte - byte */
        AutoTypeConvertProvider.registerConverter(Byte.class, byte.class, new ITypeConvert() {
            @Override
            public Object convert(Object t){
                if(t == null)
                    return 0;
                return ((Byte)(t)).byteValue();
            }
        });
        AutoTypeConvertProvider.registerConverter(byte.class, Byte.class, new ITypeConvert() {
            @Override
            public Object convert(Object t)  {
                return (Byte)t;
            }
        });

        /*Float - float */
        AutoTypeConvertProvider.registerConverter(Float.class, float.class, new ITypeConvert() {
            @Override
            public Object convert(Object t)  {
                if(t == null)
                    return 0;
                return ((Float)(t)).floatValue();
            }
        });
        AutoTypeConvertProvider.registerConverter(float.class, Float.class, new ITypeConvert() {
            @Override
            public Object convert(Object t) {
                return (Float)t;
            }
        });

        /*Double - double */
        AutoTypeConvertProvider.registerConverter(Double.class, double.class, new ITypeConvert() {
            @Override
            public Object convert(Object t)  {
                if(t == null)
                    return 0;
                return ((Double)(t)).doubleValue();
            }
        });
        AutoTypeConvertProvider.registerConverter(double.class, Double.class, new ITypeConvert() {
            @Override
            public Object convert(Object t)  {
                return (Double)t;
            }
        });

        /*Long - long */
        AutoTypeConvertProvider.registerConverter(Long.class, long.class, new ITypeConvert() {
            @Override
            public Object convert(Object t)  {
                if(t == null)
                    return 0;
                return ((Long)(t)).longValue();
            }
        });
        AutoTypeConvertProvider.registerConverter(long.class, Long.class, new ITypeConvert() {
            @Override
            public Object convert(Object t)  {
                return (Long)t;
            }
        });

        /*Boolean - boolean */
        AutoTypeConvertProvider.registerConverter(Boolean.class, boolean.class, new ITypeConvert() {
            @Override
            public Object convert(Object t) {
                if(t == null)
                    return 0;
                return ((Boolean)(t)).booleanValue();
            }
        });
        AutoTypeConvertProvider.registerConverter(boolean.class, Boolean.class, new ITypeConvert() {
            @Override
            public Object convert(Object t)  {
                return (Boolean)t;
            }
        });

        /*Short - short */
        AutoTypeConvertProvider.registerConverter(Short.class, short.class, new ITypeConvert() {
            @Override
            public Object convert(Object t)  {
                if(t == null)
                    return 0;
                return ((Short)(t)).shortValue();
            }
        });
        AutoTypeConvertProvider.registerConverter(short.class, Short.class, new ITypeConvert() {
            @Override
            public Object convert(Object t)  {
                return (Short)t;
            }
        });

        /*字符串兼容转换*/
        /*Integer */
        AutoTypeConvertProvider.registerConverter(String.class, Integer.class, new ITypeConvert() {
            @Override
            public Object convert(Object t)  {
                return Integer.valueOf(ConvertUtil.objToStr(t)) ;
            }
        });

        /*Byte */
        AutoTypeConvertProvider.registerConverter(String.class, Byte.class, new ITypeConvert() {
            @Override
            public Object convert(Object t)  {
                return  Byte.valueOf(ConvertUtil.objToStr(t));
            }
        });

        /*Float */
        AutoTypeConvertProvider.registerConverter(String.class, Float.class, new ITypeConvert() {
            @Override
            public Object convert(Object t) {
                return Float.valueOf(ConvertUtil.objToStr(t));
            }
        });

        /*Double*/
        AutoTypeConvertProvider.registerConverter(String.class, Double.class, new ITypeConvert() {
            @Override
            public Object convert(Object t) {
                return Double.valueOf(ConvertUtil.objToStr(t));
            }
        });

        /*Long*/
        AutoTypeConvertProvider.registerConverter(String.class, Long.class, new ITypeConvert() {
            @Override
            public Object convert(Object t)  {
                return Long.valueOf(ConvertUtil.objToStr(t));
            }
        });

        /*Boolean */
        AutoTypeConvertProvider.registerConverter(String.class, Boolean.class, new ITypeConvert() {
            @Override
            public Object convert(Object t) {
                return Boolean.valueOf(ConvertUtil.objToStr(t));
            }
        });

        /*Short */
        AutoTypeConvertProvider.registerConverter(String.class, Short.class, new ITypeConvert() {
            @Override
            public Object convert(Object t)  {
                return Short.valueOf(ConvertUtil.objToStr(t));
            }
        });

        /*String->两层类型转换支持*/
        HashMap<ConvertInfo,ITypeConvert> converts = new HashMap();
        for(final ConvertInfo convertInfo : AutoTypeConvertProvider.Converters.keySet())
        {
            if(convertInfo.fromType != String.class)
                continue;
            for(final ConvertInfo toConvertInfo : AutoTypeConvertProvider.Converters.keySet())
            {
                if(convertInfo.toType == toConvertInfo.fromType)
                {
                    ConvertInfo info = new ConvertInfo();info.fromType = convertInfo.fromType;info.toType = toConvertInfo.toType;
                    if(!AutoTypeConvertProvider.Converters.containsKey(info)) {
                        converts.put(info, new ITypeConvert() {
                            @Override
                            public <T> T convert(Object t) {
                                Object temp = AutoTypeConvertProvider.Converters.get(convertInfo).convert(t);
                                return AutoTypeConvertProvider.Converters.get(toConvertInfo).convert(temp);
                            }
                        });
                    }
                }
            }
        }

        for (Map.Entry<ConvertInfo,ITypeConvert> convert:converts.entrySet())
        {
            AutoTypeConvertProvider.registerConverter(convert.getKey().fromType, convert.getKey().toType,convert.getValue());
        }

         /*跨类型兼容转换*/
         /*Short - byte */
        AutoTypeConvertProvider.registerConverter(Short.class, byte.class, new ITypeConvert() {
            @Override
            public Object convert(Object t) {
                if(t == null)
                    return 0;
                return ((Short)(t)).byteValue();
            }
        });
        AutoTypeConvertProvider.registerConverter(byte.class, Short.class, new ITypeConvert() {
            @Override
            public Object convert(Object t) {
                return new Short((Byte)t);
            }
        });

    }

}
