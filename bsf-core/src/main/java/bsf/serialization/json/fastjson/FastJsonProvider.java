package bsf.serialization.json.fastjson;

import bsf.serialization.json.system.JsonSerialize;
import com.alibaba.fastjson.JSON;

/**
 * Created by chejiangyi on 2016/3/18.
 * fastjson 二次封装及简化使用
 */
public class FastJsonProvider implements JsonSerialize {
    public String serialize(Object obj) {
        try {
            return JSON.toJSONString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public <T> T deserialize(String json, Class<T> cls) {
        try {

            return JSON.parseObject(json, cls);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
