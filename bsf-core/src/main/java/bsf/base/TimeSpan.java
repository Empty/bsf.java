package bsf.base;

/**
 * Created by Administrator on 2016/3/7.
 */
import java.util.Date;

public class TimeSpan
{
    public final static TimeSpan ZERO = new TimeSpan(0);

    private long totalMilliSeconds = 0;

    public TimeSpan(long totalMilliSeconds)
    {
        this.totalMilliSeconds = totalMilliSeconds;
    }

    public TimeSpan(Date afterDate, Date beforeDate)
    {
        this(afterDate.getTime() - beforeDate.getTime());
    }

    public long getMilliSeconds()
    {
        return totalMilliSeconds;
    }
    public long  getSeconds()
    {
        return  Math.round(totalMilliSeconds/1000);
    }
    public long getMinutes()
    {
        return  Math.round(totalMilliSeconds/(1000*60));
    }
    public long getHours()
    {
        return  Math.round(totalMilliSeconds/(1000*60*60));
    }
}

