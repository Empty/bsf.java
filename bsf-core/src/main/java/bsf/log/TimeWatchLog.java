package bsf.log;

import bsf.base.BsfConfig;
import bsf.log.base.TimeWatchLogConfig;
import bsf.util.DateUtil;
import bsf.util.FileUtil;
import org.joda.time.DateTime;

import java.io.File;
import java.util.Date;

/**
 * Created by chejiangyi on 2017/3/28.
 */
public class TimeWatchLog {
    Date startTime;
    public  TimeWatchLog()
    {
        startTime = new Date();
    }

    public void write(String msg)
    {
        if (!TimeWatchLogConfig.getConfig().IsWriteLog)
            return;

        if (TimeWatchLogConfig.getConfig().LocalFile)
        {
            String message = DateUtil.toString(new Date(),"yyyy-MM-dd HH:mm:ss:SSS") + " " + msg + "【耗时】: " + DateUtil.totalSeconds(DateUtil.subtract(new Date(),startTime))+" s"+ FileUtil.lineSeparator();
            String filepath = FileUtil.getFilePath(FileUtil.getDirectoryPath(TimeWatchLog.class), "timewatchlog", DateUtil.toString(new Date(),"yyyy-MM-dd") + ".time.log");
            FileUtil.createDirectory(filepath);
            FileUtil.appendAllText(filepath,message);
        }
        if (TimeWatchLogConfig.getConfig().MonitorPlatform)
        {


        }
    }
}
