package bsf.log.base;

/**
 * Created by chejiangyi on 2017/3/29.
 */
public abstract class BaseLog {
    protected static String getTypeName(Class cls)
    {
        return cls == null ? "" : "【" + cls.getName() + "】";
    }
}
