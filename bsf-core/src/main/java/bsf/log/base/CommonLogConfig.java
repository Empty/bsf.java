package bsf.log.base;

import bsf.base.BsfConfig;
import bsf.system.configparser.ConfigStringParserManager;

/**
 * Created by chejiangyi on 2017/3/31.
 */
public class CommonLogConfig extends BaseLogConfig {
    /**
     * 是否写入监控平台
     */
    public boolean MonitorPlatform = false;

    /**
     * 是否写入本地文件
     */
    public boolean LocalFile = true;

    public static CommonLogConfig getConfig() {
        return new ConfigStringParserManager().GetParser(CommonLogConfig.class).Parse(BsfConfig.getCommonLog(), CommonLogConfig.class);
    }
}