package bsf.log.base;

import bsf.base.BsfConfig;
import bsf.system.configparser.ConfigStringParserManager;

/**
 * Created by chejiangyi on 2017/3/31.
 */
public class ErrorLogConfig extends BaseLogConfig {
    /**
     * 是否写入监控平台
     */
    public boolean MonitorPlatform = true;

    /**
     * 是否写入本地文件
     */
    public boolean LocalFile=true;
    /**
    * 是否打印BSF堆栈
    * */
    public boolean IsPrintBsfStackTrace = true;

    public static ErrorLogConfig getConfig()
    {
        return new ConfigStringParserManager().GetParser(ErrorLogConfig.class).Parse(BsfConfig.getErrorLog(),ErrorLogConfig.class);
    }
}
