package bsf.log.base;

import bsf.base.BsfConfig;
import bsf.system.configparser.ConfigStringParserManager;

/**
 * Created by chejiangyi on 2017/3/31.
 */
public class TimeWatchLogConfig extends BaseLogConfig {
    /**
     * 是否写入监控平台
     */
    public boolean MonitorPlatform = true;

    /**
     * 是否写入本地文件
     */
    public boolean LocalFile=true;

    public static TimeWatchLogConfig getConfig()
    {
        return new ConfigStringParserManager().GetParser(TimeWatchLogConfig.class).Parse(BsfConfig.getTimeWatchLog(),TimeWatchLogConfig.class);
    }
}
