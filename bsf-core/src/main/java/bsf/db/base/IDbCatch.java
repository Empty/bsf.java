package bsf.db.base;

/**
 * Created by Administrator on 2016/3/25.
 */
/*
Db拦截器 如错误，耗时
* */
public interface IDbCatch<T> {
    T invoke() throws Exception;
}
