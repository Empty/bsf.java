package bsf.db.base;

/**
 * Created by chejiangyi on 2016/3/23.
 */
// 摘要:
//     C# 指定查询内的有关 System.Data.DataSet 的参数的类型。(兼容C# clr自带类)
public enum ParameterDirection {
    // 摘要:
    //     参数是输入参数。
    INPUT(1),
    //
    // 摘要:
    //     参数是输出参数。
    OUTPUT(2);

    public int value;
    private ParameterDirection(int value)
    {
        this.value = value;
    }
}

