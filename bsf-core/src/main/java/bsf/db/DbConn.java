package bsf.db;

import bsf.db.base.*;
import bsf.db.pool.ConnectionPoolHelper;
import bsf.log.ErrorLog;


import java.sql.Connection;

/**
 * Created by chejiangyi on 2016/3/23.
 * 用于兼容C#数据库操作及使用方式兼容
 */
    public class DbConn extends DbConnBase
    {
        protected DbConn(Connection conn)
        {
            super(conn);
        }

        /** 创建连接
         * @connectstring 模板:type=sqlserver;pooltype=druid;driverClassName=com.microsoft.sqlserver.jdbc.SQLServerDriver;url=jdbc:sqlserver://192.168.1.209:1433[;]DatabaseName[=]dyd_bs_task;username=sa;password=111
         @return
         */
        public static DbConn createConn(String connectstring)
        {
            try
            {
                Connection conn = ConnectionPoolHelper.getConnection(connectstring);
                DbConn cn = new DbConn(conn);
                return cn;
            }
            catch(Exception e)
            {
                throw new DbException("createConn",e,DbConn.class);
            }
        }

    }



