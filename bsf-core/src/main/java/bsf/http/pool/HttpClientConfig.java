package bsf.http.pool;

/**
 * Created by chejiangyi on 2017/3/31.
 */
public class HttpClientConfig {
    //允许单个host（路由）的请求数==连接最大数
    public int DefaultMaxPerRoute = 500;                                         // 每个路由的默认最大连接数
    public int DefaultMaxTotal     = 500;                                         // 连接池里的最大连接数
    public int MaxPendingConnect = 50;                                           //最大等待请求数

    public int ConnectionRequestTimeout =5000;//请求超时时间
    public int SocketTimeout = 10000;//socket超时时间
    public int ConnectTimeout = 10000;//连接超时时间
    public boolean RedirectsEnabled = false;//

    @Override
    public int hashCode()
    {
        return (DefaultMaxPerRoute+"-"+DefaultMaxTotal+"-"+MaxPendingConnect+"-"+ConnectionRequestTimeout+"-"
                +SocketTimeout+"-"+ConnectTimeout+"-"+RedirectsEnabled).hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        return this.hashCode()==obj.hashCode();
    }
}
