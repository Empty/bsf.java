package bsf.util;

import org.apache.commons.lang3.StringUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by chejiangyi on 2017/3/28.
 */
public class StringUtil {

    public static String nullToEmpty(String str) {
        return str != null ? str : "";
    }

    // 部分字符串获取
    public static String subString2(String str, int maxlen)
    {
        if (StringUtils.isEmpty(str))
            return str;
        if (str.length() <= maxlen)
            return str;
        return StringUtils.substring(str,0,maxlen);
    }

}
