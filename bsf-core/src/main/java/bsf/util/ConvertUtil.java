package bsf.util;

import bsf.system.typeconvert.AutoTypeConvertProvider;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by chejiangyi on 2017/3/30.
 */
public class ConvertUtil {

    /** 【通用函数】通用类型转换,不断完善中
     */
    public static  <T> T convert(Object obj, Class<T> totype) {
        return new AutoTypeConvertProvider().convert(obj,totype);
    }

    /** 【通用函数】object类型转换为byte类型

     */
    public static Byte objToByte(Object aObj)
    {
        if (aObj == null)
        {
            return 0;
        }
        try
        {

            if(aObj instanceof Number)
            {
                return ((Number)aObj).byteValue();
            }
        }
        catch (java.lang.Exception e)
        {

        }
        return 0;
    }

    /** 【通用函数】object类型转换为short类型

     */
    public static short objToShort(Object aObj)
    {
        if (aObj == null)
        {
            return 0;
        }
        try
        {
            if(aObj instanceof Number)
            {
                return ((Number)aObj).shortValue();
            }
        }
        catch (java.lang.Exception e)
        {

        }
        return 0;
    }

    /** 【通用函数】object类型转换为int类型

     */
    public static int objToInt(Object aObj)
    {
        if (aObj == null)
        {
            return 0;
        }
        try
        {
            if(aObj instanceof Number)
            {
                return ((Number)aObj).intValue();
            }
        }
        catch (java.lang.Exception e)
        {

        }
        return 0;
    }

    /** 【通用函数】object类型转换为Int64类型

     */
    public static long objToInt64(Object aObj)
    {
        if (aObj == null)
        {
            return 0;
        }
        try
        {
            if(aObj instanceof Number)
            {
                return ((Number)aObj).longValue();
            }
        }
        catch (java.lang.Exception e)
        {

        }
        return 0;
    }

    /** 【通用函数】object类型转换为double类型

     */
    public static double objToDouble(Object aObj)
    {
        if (aObj == null)
        {
            return 0;
        }
        try
        {
            if(aObj instanceof Number)
            {
                return ((Number)aObj).doubleValue();
            }
        }
        catch (java.lang.Exception e)
        {

        }
        return 0;
    }

    /** 【通用函数】object类型转换为decimal类型

     */
    public static BigDecimal objToDecimal(Object aObj)
    {
        if (aObj == null)
        {
            return BigDecimal.ZERO;
        }
        try
        {
            if(aObj instanceof java.math.BigDecimal) {
                return (java.math.BigDecimal)aObj;
            }
        }
        catch (java.lang.Exception e)
        {

        }
        return BigDecimal.ZERO;
    }

    /** 【通用函数】object类型转换为float类型

     */
    public static float objToFloat(Object aObj)
    {
        if (aObj == null)
        {
            return 0;
        }
        float dRet = 0;

        try
        {
            if(aObj instanceof Number)
            {
                return ((Number)aObj).floatValue();
            }
        }
        catch (java.lang.Exception e)
        {

        }
        return dRet;
    }


    /** 【通用函数】object类型转换为datetime类型

     */
    public static Date objToDateTime(Object aObj)
    {
        java.util.Date dRet = new java.util.Date(0);
        if (aObj == null)
        {
            return dRet;
        }

        try
        {
            String str = nullToStr(aObj);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS");
            return sdf.parse(str);
        }
        catch (java.lang.Exception e)
        {

        }
        return dRet;
    }

    /** 【通用函数】object类型转换为bool类型，直接强制转换(bool)aObj

     */
    public static boolean objToBool(Object aObj)
    {
        if (aObj == null)
        {
            return false;
        }
        if (aObj instanceof Boolean)
        {
            return ((Boolean)aObj).booleanValue();
        }
        if (aObj.toString().equals("true"))
        {
            return true;
        }
        if(aObj instanceof Number)
        {
            byte b = ((Number)aObj).byteValue();
            if (b == 1)
                return true;
            else
                return false;
        }
        return false;

    }

    /** 【通用函数】object类型转换为string类型

     */
    public static String nullToStr(Object aObj)
    {
        return (aObj == null?"":String.valueOf(aObj));
    }
    /** 【通用函数】相当于NullToStr

     */
    public static String objToStr(Object aObj) {
        return (aObj == null ? null : String.valueOf(aObj));
    }


    public static Long strToInt64(Object aObj)
    {
        try {
            return Long.parseLong(objToStr(aObj));
        } catch(Exception e) {}
        return 0L;
    }

    public static Integer strToInt(Object aObj)
    {
        try {
            return Integer.parseInt(objToStr(aObj));
        } catch(Exception e) {}
        return 0;
    }
}
