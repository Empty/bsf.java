package bsf.util;

import bsf.base.BsfException;
import org.apache.commons.lang3.StringUtils;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by chejiangyi on 2017/3/29.
 */
public class DateUtil {
    public static String toString(Date date)
    {
        return toString(date,"yyyy-MM-dd HH:mm:ss");
    }
    public static String toString(Date date, String format) {
        SimpleDateFormat f = new SimpleDateFormat(format);
        return f.format(date);
    }
    public static Date getDate(Date time){
        SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
        return parse(formatter.format(time),"yyyyMMdd");
    }

    public static Date parse(String strDate, String pattern)
    {
        try {
            return StringUtils.isEmpty(strDate) ? null : new SimpleDateFormat(pattern).parse(strDate);
        }
        catch (Exception exp)
        {
            throw new BsfException("parse出错",exp,DateUtil.class);
        }
    }

    public static long subtract(Date from, Date to) {
        return from.getTime() - to.getTime();
    }

    public static long totalSeconds(long time)
    {
        return time/1000;
    }

    public static long totalMinutes(long time)
    {
        return totalSeconds(time)/60;
    }

    public static long totalHours(long time)
    {
        return  totalMinutes(time)/60;
    }

    public static long totalDays(long time)
    {
        return totalHours(time)/24;
    }

}
