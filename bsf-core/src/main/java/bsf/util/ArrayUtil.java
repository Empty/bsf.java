package bsf.util;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;

/**
 * Created by chejiangyi on 2017/4/20.
 */
public class ArrayUtil {
    public static <T extends Comparable<? super T>> List<T> sort(List<T> list) {
        Collections.sort(list);
        return list;
    }

    public static List distinct(List list) {
        return new ArrayList(new HashSet(list));
    }
}
