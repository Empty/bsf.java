package bsf.baseservice.configmanager;

/**
 * Created by chejiangyi on 2017/4/5.
 */

import bsf.base.RefObject;

/**
 定义配置中心操作接口

 */
public interface IConfigManagerProvider
{
    /**
     获取配置

     <typeparam name="T"></typeparam>
     @param configkey 配置的key（配置项）
     @return
     */
    <T> T get(String configkey,Class<T> cls);

    /**
     判断配置是否存在配置

     @param configkey 配置的key（配置项）
     @param value 传出的value值
     @return
     */
    <T> boolean tryGet(String configkey,Class<T> cls, RefObject<T> value);
}